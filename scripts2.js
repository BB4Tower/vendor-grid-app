// Get HTML head element 
	var head = document.getElementsByTagName('HEAD')[0];  
	var body = document.getElementsByTagName('BODY')[0]; 
	
	var link = document.createElement('link');   
	link.rel = 'stylesheet';    
	link.type = 'text/css';   
	link.href = 'https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css';  
	head.appendChild(link); 
	
	var link2 = document.createElement('link');   
	link2.rel = 'stylesheet';    
	link2.type = 'text/css';   
	link2.href = 'https://unpkg.com/ag-grid-community/dist/styles/ag-theme-material.css';  
	head.appendChild(link2)
	
	var link3 = document.createElement('link');   
	link3.rel = 'stylesheet';    
	link3.type = 'text/css';   
	link3.href = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/stylev.css';  
	head.appendChild(link3)
	
	var link4 = document.createElement('link');   
	link4.rel = 'stylesheet';    
	link4.type = 'text/css';   
	link4.href = 'https://use.fontawesome.com/releases/v5.6.3/css/all.css';  
	head.appendChild(link4)
	
    var style1 = document.createElement('style');	
	var primary_color = document.getElementById("4TProdCatalog").getAttribute("data-p-color");
	var secondary_color = document.getElementById("4TProdCatalog").getAttribute("data-s-color");
	style1.innerHTML = 'h2 {background-color: ' + primary_color + '!important; color: ' + secondary_color + '!important;}.ag-theme-material .ag-row {color:' + primary_color + ';} .fa-plus-circle {color: ' + primary_color + ';} .fa-minus-circle {color: ' + primary_color + ';} .fa-circle {color:' + secondary_color + ';}.btn-info{color: ' + secondary_color + ';background-color: ' + primary_color + '!important;} .modal-dialog.modal-notify.modal-info .modal-header {color: ' + secondary_color + ';background-color: ' + primary_color + '!important;}';  
	head.appendChild(style1)
	
	var link4 = document.createElement('link');   
	link4.rel = 'stylesheet';    
	link4.type = 'text/css';   
	link4.href = 'https://fonts.googleapis.com/css?family=Roboto';  
	head.appendChild(link4)
	
	var script1 = document.createElement('script');     
	script1.src = 'https://unpkg.com/ag-grid-enterprise/dist/ag-grid-enterprise.min.noStyle.js';  
	head.appendChild(script1)
	
	var script2 = document.createElement('script');     
	script2.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/0b779722a0f8bfdb330b9c0e463db2a2d401c1cc/scripts.js';  
	head.appendChild(script2)
	
	body.onload = function(){loadGrid()};
	
	var myDiv = document.getElementById('4TProdCatalog'); 
		
	var header1 = document.createElement('h2');   
	header1.setAttribute('class', 'color-block primary-color text-white z-depth-1 text-right');
	header1.setAttribute('data-role', 'gridHeader');	
	header1.xmlns = 'http://www.w3.org/1999/html';
	header1.innerHTML = 'Products <img src="img/4TowerSqLogo.png" style="height:35px;"/>';	
	myDiv.appendChild(header1);
	
	var grid1 = document.createElement('div');   
	grid1.setAttribute('class', 'ag-theme-material');
	grid1.id = 'myGrid';	
	grid1.style = 'height: 600px;width:100%;';
	
	myDiv.appendChild(grid1);
	
	var foot = document.createElement('div');   
	foot.setAttribute('class', 'containerFoot');
	foot.innerHTML = '<span class="text-muted"> Powered by <img src="https://dev.4-tower.com/dev/specadvisor/assets/images/4Tower.png" style="width:80px;"></span>'
	myDiv.appendChild(foot); 
	
	var infomodal = document.createElement('div');   
	infomodal.setAttribute('class', 'modal hide');
	infomodal.id = 'g4dl78t';	
	infomodal.style = 'padding-right: 17px;';
	infomodal.innerHTML = '<div class="modal-dialog modal-notify text-white modal-md modal-info"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="modal-title"></h4></div><div class="modal-body text-center"><img class="modalImage"></div><div class="modal-footer"><button class="btn btn-info ng-star-inserted" type="button" onclick="showInfoModal()">Close </button></div></div></div>';
	body.appendChild(infomodal); 
	
	var backdrop = document.createElement('div');   
	backdrop.setAttribute('class', 'modal-backdrop hide');
	backdrop.id = '3edfr4t';
	body.appendChild(backdrop); 

	