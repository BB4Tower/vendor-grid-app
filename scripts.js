  function loadGrid() {
	
// specify the columns
    var columnDefs = [	
	  {headerName: "4Tower ID", field: "name", resizable: true, /*sortable: true/*,
		cellRenderer:'agGroupCellRenderer',
        cellRendererParams: {
            checkbox: true
        }*/},
	  {headerName: "Image", field: "id", resizable: true, /*sortable: false, */width: 105, cellRenderer: 'imgCellRenderer', menuTabs: []},
      {headerName: "Category", field: "product_category", rowGroup: true, resizable: true, /*sortable: true,*/ hide: true},
	  {headerName: "Model", field: "model", resizable: true, /*sortable: true*/},
	  {headerName: "Catalog #", field: "manufacturer_part_number", resizable: true, /*sortable: true*/},
	  /*{headerName: "Price", field: "price", resizable: true, sortable: true},*/
	  {headerName: "Documents", field: "documents", resizable: true, /*sortable: true, */cellRenderer: 'docCellRenderer', width: 400,menuTabs: []}
    ];

    var autoGroupColumnDef = {
        //headerName: "Category",
        //field: "name",
		width: 120,
		resizable: true,
		//sortable: true,
		sort: 'asc',
		/*cellRenderer:'agGroupCellRenderer',
        cellRendererParams: {
            checkbox: true
        },*/
		menuTabs: [],
		groupSelectsChildren: true
		//rowGroup: true,
    }

    // let the grid know which columns and what data to use
    var gridOptions = {
      columnDefs: columnDefs,
      autoGroupColumnDef: autoGroupColumnDef,
	  components: {
        'imgCellRenderer': IMGCellRenderer,
		'docCellRenderer': DOCCellRenderer
      },
	  suppressContextMenu: true,
	  /*groupSelectsChildren: true,*/
	  defaultColDef: {
		menuTabs: ['filterMenuTab'],
		movable: false,
		filter: true,
		suppressMovable: true
	  },
	  suppressRowClickSelection: true,
      /*rowSelection: 'multiple',*/
	  icons: {
			// use font awesome for menu icons	
			// use some strings from group
			groupExpanded:
			  '<span class="fa-stack fa-1x" style="font-size:20px;"><i class="fas fa-circle fa-stack-1x"></i><i class="fa fa-minus-circle fa-stack-1x"></i></span>',
			groupContracted:
			  '<span class="fa-stack fa-1x" style="font-size:20px;"><i class="fas fa-circle fa-stack-1x"></i><i class="fa fa-plus-circle fa-stack-1x"></i></span>',
             },
	  statusBar: {
		statusPanels: [
		  { statusPanel: 'agTotalAndFilteredRowCountComponent', align: 'left' },
		  { statusPanel: 'agTotalRowCountComponent', align: 'center' },
		  { statusPanel: 'agFilteredRowCountComponent' },
		  { statusPanel: 'agSelectedRowCountComponent' },
		  { statusPanel: 'agAggregationComponent' },
		],
	  },
	  //groupUseEntireRow: true
    };
  
  // cell renderer class
	function IMGCellRenderer() {
	}

	// init method gets the details of the cell to be renderer
	IMGCellRenderer.prototype.init = function(params) {
		if (!params.node.group && params.value) {
			console.log(params);
			this.eGui = document.createElement('img');
			this.eGui.src = 'https://dev.4-tower.com/dev/specdata/upload/' + params.value + '_product_image';
			this.eGui.style = 'height: 48px;';
			this.eGui.setAttribute('onclick', 'showInfoModal("https://dev.4-tower.com/dev/specdata/upload/' + params.value + '_product_image","' + params.data.name + '");');
			this.eGui.setAttribute('onerror', 'this.style.display="none"');
		} 
	};

	IMGCellRenderer.prototype.getGui = function() {
		return this.eGui;
	};
	
	function DOCCellRenderer() {
	}

	// init method gets the details of the cell to be renderer
	DOCCellRenderer.prototype.init = function(params) {
		if (!params.node.group && params.value) {
			this.eGui2 = document.createElement('div');
			Object.values(JSON.parse(params.value)).forEach(element => {
				
				this.eGui1 = document.createElement('a');
				this.eGui1.href = 'https://dev.4-tower.com/dev/specdata/upload/' + element.revision_id;
				this.eGui1.target = '_blank';
				this.eGui = document.createElement('img');
				switch(element.document_type) {
				  case 'Cut Sheet':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/cs.png';
					this.eGui.style = 'height: 40px;padding-right:2px;padding-top:2px;';
					break;
				  case 'User manual':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/um.png';
					this.eGui.style = 'height: 40px;padding-right:2px;padding-top:2px;';
					break;
				  case 'Installation Document':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/id.png';
					this.eGui.style = 'height: 40px;padding-right:2px;padding-top:2px;';
					break;
				  case 'Brochure':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/br.png';
					this.eGui.style = 'height: 40px;padding-right:2px;padding-top:2px;';
					break;
				  case 'Seismic':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/se.png';
					this.eGui.style = 'height: 40px;padding-right:2px;padding-top:2px;';
					break;
				  case 'revit2015':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r15.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  case 'revit2016':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r16.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  case 'revit2017':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r17.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  case 'revit2018':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r18.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  case 'revit2019':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r19.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  case 'revit2020':
					this.eGui.src = 'https://bitbucket.org/BB4Tower/vendor-grid-app/raw/90b41304746900fab40b036d3f29f8d20e2350a8/img/r20.png';
					this.eGui.style = 'height: 35px;padding-right:2px;';
					break;
				  default:
					// code block
				}
				this.eGui.setAttribute('onerror', 'this.style.display="none"');
				this.eGui1.appendChild(this.eGui);
				this.eGui2.appendChild(this.eGui1);
			});
		} 
	};

	DOCCellRenderer.prototype.getGui = function() {
		return this.eGui2;
	};
	
	
  // lookup the container we want the Grid to use
  var eGridDiv = document.querySelector('#myGrid');

  // create the grid passing in the div to use together with the columns & data we want to use
  new agGrid.Grid(eGridDiv, gridOptions);
	
  var url = "https://dev.4-tower.com/dev/specdata/custom/service/v4_1/rest.php?method=get_docs";
  var url1 = "https://dev.4-tower.com/dev/specdata/custom/service/v4_1/rest.php?method=user_login";
  var vGUID = document.getElementById("4TProdCatalog").getAttribute("data-vendor");
  
var xhttp1 = new XMLHttpRequest();
  xhttp1.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//console.log(Object.values(JSON.parse(this.responseText)));
      var xhttp = new XMLHttpRequest();	
	  xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		  //console.log(Object.values(Object.values(JSON.parse(this.responseText).modules)[0].docs));
		  gridOptions.api.setRowData(Object.values(Object.values(JSON.parse(this.responseText).modules)[0].docs));
		}
	  };
	  xhttp.open("POST", url, true);
	  xhttp.setRequestHeader("Content-type", "application/json");
	  //console.log();
	  xhttp.send("{\"docTypes\":[\"EXOPP_EquiProducts\"],\"session\":\"" + Object.values(JSON.parse(this.responseText))[2].id + "\",\"userId\":\"" + Object.values(JSON.parse(this.responseText))[2].name_value_list.user_id.value + "\",\"options\":{\"filters\":{\"deleted\":{\"value\":\"0\",\"operator\":\"=\",\"type\":\"number\"},\"exopp_equiaccounts_id_c\":{\"value\":\"" + vGUID + "\",\"operator\":\"=\",\"type\":\"string\"}}}}");
    }
  };
  xhttp1.open("POST", url1, true);
  xhttp1.setRequestHeader("Content-type", "text/plain");
  xhttp1.send("{\"userName\":\"testing\",\"password\":\"testtest123!\"}");
  }

function showInfoModal(theImage = "empty", theName = "empty") {
	var el1 = document.getElementById("g4dl78t");
	
	if (theImage != 'empty') {
		el1.getElementsByTagName("img")[0].src = theImage;
	}
	if (theName != 'empty') {
		console.log(theName);
		var el3 = el1.getElementsByTagName("h4");
		el3[0].innerHTML = theName;
	}
	el1.classList.toggle("show");
	el1.classList.toggle("hide");
	
	var el2 = document.getElementById("3edfr4t");
	el2.classList.toggle("show");
	el2.classList.toggle("hide");
	
	//setimage
}